require "./Element"
# ==========
# = Keller =
# ==========
# FILO = First In, Last Out
# Keller, Stapel, engl. Stack

class Stack
  def initialize
    @top = nil
  end
  
  # push = einfügen
  def push(wert)
    e = Element.new(wert)
    
    if empty?
      @top = e
    else
      e.next = @top
      @top = e
    end
  end
  
  # pop = entfernen
  def pop
    if empty?
      raise "Error"
    else
      value = @top.value
      @top = @top.next
      return value
    end
  end
  
  def empty?
    return @top == nil
  end
  
  # top = oberstes zurückgeben
  def top
    if empty?
      raise "Missile launch"
    else
      return @top.value
    end
  end
  
  def size
    if empty?
      return 0
    else
      count = 0
      runner = @top
      
      while runner.next
        runner = runner.next
        count += 1
      end
      
      return count
    end
  end
  
  def contains?(value)
    runner = @top
    
    until runner == nil
      if runner.value == value
        return true
      end
      
      runner = runner.next
    end
    
    return false
  end
  
  def show
    if empty?
      return "[]"
    else
      output = "["
      runner = @top
      until runner == nil
        output += "#{runner.value}-"
        runner = runner.next
      end
      
      return output + "]"
    end
  end
end
