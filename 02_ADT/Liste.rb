require "./Schlange"

class Liste < Schlange
  def einfuegen(wert, index = self.groesse)
    if index > self.groesse
      raise "Ungueltiger Index!"
    elsif index == 0
      neues = Element.new(wert)
      neues.naechstes = @erstes
      @erstes = neues
    else
      neues = Element.new(wert)
      laeufer = @erstes
      
      (index-1).times do
        laeufer = laeufer.naechstes
      end

      neues.naechstes = laeufer.naechstes
      laeufer.naechstes = neues
    end
  end
  
  def element_an(index)
    if index >= self.groesse
      raise "Ungueltiger Index!"
    else
      laeufer = @erstes
      index.times do
        laeufer = laeufer.naechstes
      end
      
      return laeufer.wert
    end
  end
  
  def index_von(wert)
    # Gibt den Index zurück von dem (ersten) Element mit dem Wert <wert>,
    # oder -1, wenn das Element nicht existiert
  end
  
  def loeschen_an(index)
    # Loescht das Element an Stelle index, gibt den Wert zurück
  end
  
  def loeschen(wert)
    # Loescht das erste Element mit dem Wert <wert>, gibt <wert> zurück,
    # oder "nil" wenn Element nicht existiert
  end
  
  def groesse
    if leer?
      return 0
    else
      zaehler = 1
      laeufer = @erstes
    
      while laeufer.naechstes
        laeufer = laeufer.naechstes
        zaehler += 1
      end
    
      return zaehler
    end
  end
  
  def ausgeben
    
  end
end
