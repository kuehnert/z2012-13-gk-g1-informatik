require "./Schlange"

s = Schlange.new

s.einfuegen("Willi")
s.einfuegen(5)
s.einfuegen(1.2)
s.einfuegen(nil)
s.einfuegen("Ende")

until s.leer?
  puts s.entfernen
end

# puts s.leer?
puts s.erstes
