Datentypen

Elementare Datentypen
=> An den Speicheradressen wird der Wert selbst gespeichert
=> int i => i steht für eine Speicheradresse (0x0D0B)
- int, long (Integer), float, boolean

Referenzdatentypen
=> An den Speicheradressen wird nur der Verweis auf die Speicheradresse im Hauptspeicher gespeichert.

- String
- Array
- alle Klassen/Objekte

Abstrakte Datentypen
- Schlangen
- Listen 
- Keller
- Bäume
- Graphen
