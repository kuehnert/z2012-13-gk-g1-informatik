require "./Stack"

def rangier_sort1(r1)
  train = Stack.new
  r2 = Stack.new
  
  gesucht = 1
  until r1.empty?
    until r1.empty?
      wagon = r1.pop
    
      if wagon == gesucht
        train.push wagon
        gesucht += 1
      else
        r2.push wagon
      end
    end

    # Gleis r1 ist leer
    r1, r2 = r2, r1 # Vertauschung der Gleise
  end
end

# 1. Gucken, auf welchem Gleis mehr Wagen stehen => Stack erweitern um eine Methode size, die die Anzahl eingekellerter Elemente zurück
def rangier_sort2(r1)
  train = Stack.new
  r2 = Stack.new
  
  gesucht = 1
  until r1.empty?
    until r1.empty?
      wagon = r1.pop
    
      if wagon == gesucht
        train.push wagon
        gesucht += 1
      
        if r2.size > r1.size
          r1, r2 = r2, r1 # Vertauschung der Gleise
        end
      else
        r2.push wagon
      end
    end

    # Gleis r1 ist leer
    r1, r2 = r2, r1 # Vertauschung der Gleise
  end
end

# 2. Stack beibringen, nach einem bestimmten Element zu suchen "contains?(value)"
def rangier_sort3(r1)
  train = Stack.new
  r2 = Stack.new
  
  gesucht = 1
  until r1.empty?
    until r1.empty?
      wagon = r1.pop
    
      if wagon == gesucht
        train.push wagon
        gesucht += 1
      
        if r2.contains?(value)
          r1, r2 = r2, r1 # Vertauschung der Gleise
        end
      else
        r2.push wagon
      end
    end

    # Gleis r1 ist leer
    r1, r2 = r2, r1 # Vertauschung der Gleise
  end
end

# Anmerkungen / Ideen zur Verbesserung
# 0. Methode, die einen zufälligen Zug mit Länge n erzeugt.
def random_train(n)
  s = Stack.new
  (1..n).to_a.shuffle.each { |e| s.push e }
  return s
end

r1 = random_train(20)
r1kopie = r1.dup
puts r1kopie.show
rangier_sort1(r1kopie)
puts r1kopie.show
rangier_sort1(r1.dup)
rangier_sort1(r1)
