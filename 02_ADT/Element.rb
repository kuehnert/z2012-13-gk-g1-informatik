class Element
  def initialize(wert)
    @wert = wert
    @naechstes = nil
  end
  
  def wert
    return @wert
  end
  
  def naechstes
    return @naechstes
  end
  
  def wert=(neuer_wert)
    @wert = neuer_wert
  end
  
  def naechstes=(naechstes)
    @naechstes = naechstes
  end
  
  alias_method :value, :wert
  alias_method :value=, :wert=
  alias_method :next, :naechstes
  alias_method :next=, :naechstes=
end

# class Element
#   attr_accessor :wert, :naechstes
# end
