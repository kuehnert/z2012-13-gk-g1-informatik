require "baum"

def baum_erzeugen
  # Top-Down Methode
  wurzel = Node.new("L")
  wurzel.left = Node.new("R")
  wurzel.left.left = Node.new("X")
  wurzel.left.right = Node.new("S")
  wurzel.right = Node.new("Z")
  wurzel.right.left = Node.new("A")
  wurzel.right.left.left = Node.new("F")
  wurzel.right.left.right = Node.new("E")
  wurzel.right.right = Node.new("C")

  # Bottom-Up Methode
  node_a = Node.new("A", Node.new("F"), Node.new("E"))
  node_z = Node.new("Z", node_a, Node.new("C"))
  node_r = Node.new("R", Node.new("X"), Node.new("S"))
  wurzel = Node.new("L", node_r, node_z)

  wurzel.ausgeben
end

def teste_einfuegen
  wurzel = Node.new('M')
  wurzel.add('A')
  wurzel.add('Z')
  wurzel.add('C')
  wurzel.add('B')
  wurzel.add('D')
  wurzel.add('H')
  wurzel.add('G')
  wurzel.add('T')
  wurzel.add('J')
  wurzel.add('P')
  wurzel.ausgeben_lnr
end

def teste_einfuegen2
  wurzel = Node.new('Z')
  wurzel.add('Y')
  wurzel.add('X')
  wurzel.add('W')
  wurzel.add('V')
  wurzel.add('U')
  wurzel.add('T')
  wurzel.add('S')
  wurzel.ausgeben_lnr
end

teste_einfuegen2
