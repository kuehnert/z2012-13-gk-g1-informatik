class Node
  attr_accessor :value, :left, :right
  
  def initialize(value, left = nil, right = nil)
    @value = value
    @left  = left
    @right = right
  end
  
  # pre-order
  def ausgeben_nlr
    puts @value # wert von knoten
    
    # linker Teilbaum
    if @left != nil
      # es gibt einen linken Teilbaum
      @left.ausgeben_nlr
    end

    # rechter Teilbaum
    if @right != nil
      @right.ausgeben_nlr
    end
  end

  # in-order
  def ausgeben_lnr
    # linker Teilbaum
    @left.ausgeben_lnr if @left != nil

    # wert von knoten
    puts @value 

    # rechter Teilbaum
    @right.ausgeben_lnr if @right != nil
  end
  
  # post-order
  def ausgeben_lrn
    # linker Teilbaum
    @left.ausgeben_lrn if @left != nil

    # rechter Teilbaum
    @right.ausgeben_lrn if @right != nil

    # wert von knoten
    puts @value 
  end

  def find(value)
    # Setze runner auf Wurzel / akt. Knoten
    runner = self
    
    until runner == nil do
      if value == runner.value
        return runner.value
      elsif value < runner.value
        runner = runner.left
      else
        runner = runner.right
      end
    end
    
    return nil
  end
  
  def add(value)
    runner = self
    
    loop do
      if value < runner.value
        if runner.left == nil
          runner.left = Node.new(value)
          return true
        else
          runner = runner.left
        end
      elsif value > runner.value
        if runner.right == nil
          runner.right = Node.new(value)
          return true
        else
          runner = runner.right
        end
      else
        # werte sind gleich
        return false
      end
    end
  end

  def leaf?
    return @left.nil? && @right.nil?
  end
end
