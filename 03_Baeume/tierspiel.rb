#!/usr/bin/env ruby -wKU
# encoding: UTF-8
require_relative "node"

class Tierspiel < Node
  def initialize
    self.value = "Ist it a mammal?"
    self.left  = Node.new("cow")
    self.right = Node.new("ant")
  end

  def new_game
    @runner = self
    @weiter = true
  end

  def ask_play_again
    puts "Would you like to play again?"
  end
  
  def spielen
    puts "Welcome to the big game!"
    new_game
    
    while @weiter do
      if @runner.leaf?
        print "Is it a #{@runner.value}? "
        antwort = gets.chomp
        
        if antwort =~ /j|y/
          puts "Hurrah, I have won! :)"
          @weiter = false
        else
          puts "Oh, I don't know your animal! :("
          print "What animal did you think of? "
          animal = gets.chomp
          
          puts "Give me a question about a difference between #{@runner.value} and #{animal}, to which the answer ‘yes’ applies to #{animal}:"
          question = gets.chomp
          @runner.left = Node.new(animal)
          @runner.right = Node.new(@runner.value)
          @runner.value = question
          
          new_game
        end
      else
        puts @runner.value
        antwort = gets.chomp

        if antwort =~ /j|y/
          @runner = @runner.left
        else
          @runner = @runner.right
        end
      end
    end
  end
end

Tierspiel.new.spielen
