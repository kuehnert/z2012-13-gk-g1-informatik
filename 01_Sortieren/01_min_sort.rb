require_relative "sort_helper"

def min_sort(a)
  # Vom Anfang bis zum Ende vom Array
  0.upto( a.size - 2 ) do |start|
    min = start
    # Finde den Index des kleinsten Elements
    # und speichere ihn in <min>
    (start + 1).upto( a.size - 1 ) do |aktuell|
      if a[aktuell] < a[min]
        min = aktuell
      end
    end
    # Vertausche Element an <min> mit Element an <start>
    swap( a, min, start )
  end
end

a = best_case_array(10)
min_sort(a)
p a

start_time = Time.now
sleep 2
end_time = Time.now
duration = end_time - start_time
puts "Es hat #{duration} Sekunden gedauert."
