require_relative "sort_helper"
# ===============================
# = Partitionieren: Algorithmus =
# ===============================
# Elementarfall:
# Das Feld besteht nur aus einem Element (und ist daher bereits sortiert)
# 
# Rekursiver Fall:
# 1. Wähle das 0. Element als Pivot-Element
# 
# 2. Partitioniere das Feld so, das "links" alle Elemente stehen, die kleiner sind als das Pivotelement und rechts alle, die größer sind.
# 
#  6|7 5 2 8 1 4 3
#    i           j  
#  6|3 5 2 8 1 4 7
#      i       j
#  6|3 5 2 8 1 4 7
#          i   j
#  6|3 5 2 4 1 8 7
#            ij
#  [1 3 5 2 4] 6 [8 7] # 0..(j-1) | (j+1)..(a.size-1)
#              ij
#  
#  6|3 5 2 4 9 8 7
#          j i
#  
# 
# 3. Wende QuickSort an auf linkes Teilfeld
#    Wende QuickSort an auf rechtes Teilfeld
# 
# 
# === Pseudocode des Partitionierens ===
# - (Pivotelement ist an Stelle 0)
# - Setze i auf 1
# - Setze j auf a.size - 1
# - Solange i < j
# -- Solange i < a.size - 1 && a[i] < Pivotelement , erhöhe i um 1
# -- Solange j > 0 && a[j] > Pivotelement, verringere j um 1
# -- Vertausche a[i] und a[j]
# - Vertausche Pivotelement mit a[j] wenn a[j] < Pivotelement

def partition!(a, start = 0, ende = a.size - 1)
  i = start + 1
  j = ende
  
  while i < j
    while i < ende && a[i] <= a[start]
      i += 1
    end
    
    while j > start && a[j] > a[start]
      j -= 1
    end
    
    if j > i 
      swap(a, i, j)
    end
  end
  
  if a[j] < a[start]
    swap(a, j, start)
  end
  
  return j
end

# =========================
# = Quicksort Algorithmus =
# =========================
# Elementarfall:
# Das Feld hat nur ein Element, oder keins
# oder hat ungültige Indizes
# => mache nix
#
# Rekursiver Fall:
# 1. Partitioniere das Feld
# => 1. Teilfeld mit "kleinen" Elementen
#    2. Pivotelement an der richtigen Stelle
#    3. Teilfeld mit "großen" Elementen

# 2. 1. Quicksort auf linkes Teilfeld
#    2. Quicksort auf rechtes Teilfeld

def quicksort!(a, start = 0, ende = a.size - 1)
  # puts "quicksort!(a, #{start}, #{ende})"
  if start < ende
    pivot = partition!(a, start, ende)
    # p a
    
    quicksort!(a, start, pivot - 1)
    quicksort!(a, pivot + 1, ende)
  end
end

a = average_case_array(5000)
# a = [50, 50, 100, 5, 7, 2, 4, 6, -100, 50] 
# start = 3, ende = 7
# p a
quicksort!(a)
# p a
