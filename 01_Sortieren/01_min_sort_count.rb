def create_random_array(elements)
  range = elements * elements
  return Array.new(elements) { rand(range) }
end

def swap(a, i, j)
  tmp = a[i]
  a[i] = a[j]
  a[j] = tmp
  
  @vertauschungen += 1
end

def min_sort!(a)
  @vergleiche = 0
  @vertauschungen = 0
  
  0.upto( a.size - 2 ) do |start|
    min = start
    (start + 1).upto( a.size - 1 ) do |aktuell|
      if a[aktuell] < a[min]
        min = aktuell
      end
      @vergleiche += 1
    end
    swap( a, min, start )
  end
end

# CSV = Comma Separated Values
output = "Elemente;Vergleiche;Vertauschungen\n"
15.times do |i|
  elements = (i+1) * 10
  a = create_random_array(elements)

  min_sort!(a)
  output << "#{elements};#@vergleiche;#@vertauschungen\n"
end

File.open("MinSortCount.csv", "w") { |io| io.puts output.gsub('.', ',') } 
`open MinSortCount.csv`
