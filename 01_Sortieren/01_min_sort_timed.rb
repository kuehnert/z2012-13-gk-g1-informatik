def create_random_array(elements)
  range = elements * elements
  return Array.new(elements) { rand(range) }
end

def swap(a, i, j)
  tmp = a[i]
  a[i] = a[j]
  a[j] = tmp
end

def min_sort!(a)
  0.upto( a.size - 2 ) do |start|
    min = start
    (start + 1).upto( a.size - 1 ) do |aktuell|
      if a[aktuell] < a[min]
        min = aktuell
      end
    end
    swap( a, min, start )
  end
end

# CSV = Comma Separated Values
output = "Elemente;Dauer in Sekunden\n"
15.times do |i|
  elements = (i+1) * 1000
  a = create_random_array(elements)

  start_time = Time.now
  min_sort!(a)
  end_time = Time.now
  duration = end_time - start_time
  output << "#{elements};#{duration}\n"
end

File.open("tabelle.csv", "w") { |io| io.puts output.gsub('.', ',') } 
`open tabelle.csv`
