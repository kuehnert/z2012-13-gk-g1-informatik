# def counting_sort(a)
#   b = Array.new(10_000, false)
#   
#   a.each do |e|
#     b[e] = true
#   end
#   
#   a = []
#   b.each_index do |i|
#     if b[i]
#       a << i
#     end
#   end
#   
#   return a
# end

def counting_sort(a)
  b = Array.new(10_000, false)
  a.each { |e| b[e] = true }

  a = []
  b.each_index { |i| a << i if b[i] }
  
  return a
end

a = [62, 63, 99, 999, 42, 40, 462, 1, 53]
p a
b = counting_sort(a)
p b

def counting_sort_b(a)
  b = Array.new(10_000, 0)
  
  a.each { |e| b[e] += 1 }

  a = []
  b.each_index do |i|
    # b[i].times { a << i }
    a += [i] * b[i]
  end
  
  return a
  
end

a = [62, 62, 99, 999, 42, 40, 62, 1, 1, 53]
p a
b = counting_sort_b(a)
p b

