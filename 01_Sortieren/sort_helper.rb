def swap(a, i, j)
  tmp = a[i]
  a[i] = a[j]
  a[j] = tmp
end

def best_case_array(n)
  # return (0..n-1).to_a
  return (0...n).to_a
end

def average_case_array(n)
  range = n * n
  return Array.new(n) { rand(range) }
end

def average_case_array(n) # !> method redefined; discarding old average_case_array
  return best_case_array(n).shuffle
end
  
def worst_case_array(n)
  return best_case_array(n).reverse
end

average_case_array(10) # => [8, 5, 6, 1, 3, 2, 0, 9, 7, 4]
worst_case_array(10) # => [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
