def finde_plateau(a)
  start = 0
  until a[start] == a[start + 1]
    start = start + 1
  end
  
  ende = start + 1
  until a[ende] != a[ende - 1]
    ende = ende + 1
  end
  
  return [start, ende - start]
end

a = [1, 2, 7, 12, 12, 12, 15, 21, 49]
# a = [12, 12, 12, 15, 21, 49]
# a = [1, 2, 7, 12, 12]
# puts finde_plateau(a)

def unterschiedliche(a)
  start, anzahl = finde_plateau(a)
  return a[0..start] + a[start+anzahl..-1]
end

def unterschiedliche2(a)
  b = a[0..0]
  
  1.upto( a.size - 1 ) do |i|
    if a[i-1] != a[i]
      b << a[i]
    end
  end
  
  return b
end

def unterschiedliche3(a)
  return a.uniq
end

puts unterschiedliche2(a)
puts unterschiedliche3(a)