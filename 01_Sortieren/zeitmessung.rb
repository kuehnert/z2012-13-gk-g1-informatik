require_relative "sort_helper.rb"

def bubble_sort_plus!(a)
	( a.size - 1).downto( 0 ) do |i|
		swaps = 0
		0.upto(i - 1) do |j|
			if a[j] > a[j + 1]
				swap(a, j, j + 1)
				swaps += 1
			end
		end

		break if swaps == 0	
	end
end

n = 10_000
start_time  = Time.now
#--------------------------------
p a = average_case_array(n)
#--------------------------------
d_p  = Time.now - start_time

start_time  = Time.now
#--------------------------------
a = average_case_array(n)
#--------------------------------
d_a  = Time.now - start_time


puts "d_p: Es hat #{d_p} Sekunden gedauert."
puts "d_a: Es hat #{d_a} Sekunden gedauert."
puts "Faktor: #{d_p/d_a}."

start_time  = Time.now
#---- Beginn der Messung -----
# Schlafe 2 Sekunden, um eine lange Operation zu simulieren
sleep(2)
#----- Ende der Messung ------
dauer = Time.now - start_time
puts "Dauer: #{dauer} Sekunden." # => "Dauer: 2.00003 Sekunden."
