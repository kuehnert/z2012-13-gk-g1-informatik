require_relative "sort_helper.rb"
# BubbleSort

# Sortiere die Zahlen mit BubbleSort: 36 42 69 4 39 7

# Regel:
# Vergleiche das aktuelle Element mit dem nächsten.
# Ist es größer, vertausche die beiden.

def bubble_sort!(a)
  (a.size - 1).downto(1) do |i|
    0.upto(i-1) do |j|
      if a[j] > a[j+1]
        swap(a, j, j+1)
      end
    end
  end
end

def bubble_sort_plus!(a)
  (a.size - 1).downto(1) do |i|
    swaps = 0
    0.upto(i-1) do |j|
      if a[j] > a[j+1]
        swap(a, j, j+1)
        swaps += 1
      end
    end

    break if swaps == 0
  end
end

p a = create_random_array(10)
bubble_sort!(a)
p a
