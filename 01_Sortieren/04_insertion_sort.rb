# 
#  04_insertion_sort.rb
#  2012-13-GK-G1-Informatik
#
require_relative "sort_helper.rb"

def insertion_sort!(a)
  1.upto(a.size-1) do |i|
    i.downto(1) do |j|
      swap(a, j, j-1) if a[j] < a[j-1]
    end
  end
end

a = %w{6 4 7 3 8 2 1 5}
insertion_sort!(a)
p a
